package cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces;

import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidlo;

public interface IJidlaRepositoryCust {
    public Jidlo getByName(String name);
}
