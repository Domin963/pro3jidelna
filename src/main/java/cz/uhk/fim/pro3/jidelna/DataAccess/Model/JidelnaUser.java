package cz.uhk.fim.pro3.jidelna.DataAccess.Model;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Entity
@Table(name = "users")
public class JidelnaUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotEmpty
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "Může obsahovat pouze písmena")
    private String name;

    @Column(nullable = false)
    @NotEmpty
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "Může obsahovat pouze písmena")
    private String surname;

    @Column(nullable = false, unique = true)
    @NotEmpty
    @Pattern(regexp="^[A-Za-z0-9]*$", message = "Může obsahovat pouze písmena a čísla")
    private String username;

    @Column(nullable = false)
    @NotEmpty
    @Size(min = 6, message = "Heslo musí mít nejméně 6 znaků")
    private String password;

    @ManyToOne()
    @JoinColumn(name = "role_id")
    private JidelnaRole role;

    @Column(nullable = false)
    private int kredit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public JidelnaRole getRole() {
        return role;
    }

    public void setRole(JidelnaRole role) {
        this.role = role;
    }

    public int getKredit() {
        return kredit;
    }

    public void setKredit(int kredit) {
        this.kredit = kredit;
    }

    public JidelnaUser() {
    }
}
