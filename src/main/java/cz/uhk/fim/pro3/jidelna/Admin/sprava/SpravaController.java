package cz.uhk.fim.pro3.jidelna.Admin.sprava;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IRoleRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IUserRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.JidelnaRole;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.JidelnaUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/Admin/sprava")
public class SpravaController {
    @Autowired
    private IUserRepository userRepo;
    @Autowired
    private IRoleRepository roleRepo;
    @Autowired
    private PasswordEncoder encoder;

    @RequestMapping(value = {"/", "", "index"})
    public String index(Model model,
                        @RequestParam("role") String role,
                        @RequestParam("page") Optional<Integer> page,
                        @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        int totalItems = 0;
        if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().equals("[zamestnanec]")) {
            role = "stravnik";
        }
        if (role.equals("stravnik")) {
            totalItems = userRepo.countAllByRole_Name(role);
        } else if (role.equals("zamestnanec")) {
            totalItems = userRepo.countAllByRole_Name(role);
        }
        int pages = (int) Math.ceil(totalItems / (double) pageSize);
        Pageable pageAble = PageRequest.of(currentPage - 1, pageSize);
        model.addAttribute("pages", pages);
        model.addAttribute("role", role);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("users", userRepo.getAllByRole_Name(role, pageAble));
        return "Admin/sprava/index";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @RequestMapping(value = "/edit")
    public String edit(Model model, @RequestParam("id") int id) {
        if (id == 0) {
            JidelnaUser user = new JidelnaUser();
            model.addAttribute("user", user);
            return "Admin/sprava/edit";
        }
        model.addAttribute("user", userRepo.getOne(id));
        return "Admin/sprava/edit";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @PostMapping(value = "/edit")
    public String edit(@Valid JidelnaUser user, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        String strerror = "";
        if (bindingResult.hasErrors()) {
            List<ObjectError> errors = bindingResult.getAllErrors();
            List<String> err = new ArrayList<>();
            if (errors.size() > 2) {
                return "Admin/sprava/edit";
            } else {
                err.add(errors.get(0).getCodes()[0]);
                err.add(errors.get(1).getCodes()[0]);
                if (err.contains("NotEmpty.jidelnaUser.password") && err.contains("Size.jidelnaUser.password")) {
                    //nezmenene heslo
                    user.setPassword(userRepo.getOne(user.getId()).getPassword());
                    userRepo.save(user);
                    redirectAttributes.addAttribute("success", true);
                    return "redirect:/Admin/sprava/index?role=" + user.getRole().getName();
                }
            }
            return "Admin/sprava/edit";
        }
        user.setPassword(encoder.encode(user.getPassword()));
        userRepo.save(user);
        redirectAttributes.addAttribute("success", true);
        return "redirect:/Admin/sprava/index?role=" + user.getRole().getName();
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @RequestMapping(value = "/create")
    public String create(Model model) {
        JidelnaUser jidelnaUser = new JidelnaUser();
        List<JidelnaRole> lstRole = new ArrayList<>();
        if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().equals("[admin]")) {
            lstRole.addAll(roleRepo.findAll());
        } else {
            jidelnaUser.setRole(roleRepo.getByName("stravnik").get());
        }
        model.addAttribute("lstRole", lstRole);
        model.addAttribute("user", jidelnaUser);
        return "Admin/sprava/create";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @PostMapping("/create")
    public String create(@Valid JidelnaUser jidelnaUser, BindingResult bindingResult) {
        jidelnaUser.setKredit(420);
        if (bindingResult.hasErrors()) {
            return "Admin/sprava/create";
        }
        jidelnaUser.setPassword(encoder.encode(jidelnaUser.getPassword()));
        userRepo.save(jidelnaUser);
        return "login";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @RequestMapping(value = "/delete")
    public String delete(@RequestParam("id") int id, @RequestParam("role") String role, RedirectAttributes redirectAttributes) {
        userRepo.deleteById(id);
        redirectAttributes.addAttribute("success", true);
        return "redirect:/Admin/sprava/index?role=" + role;
    }
}
