package cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces;

import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidlo;
import org.springframework.stereotype.Repository;

@Repository
public interface IJidlaRepository extends ExtRepo<Jidlo, Integer>, IJidlaRepositoryCust {
}
