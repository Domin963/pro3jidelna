package cz.uhk.fim.pro3.jidelna.index;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IRoleRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IUserRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.JidelnaUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class HomeController {
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IRoleRepository roleRepository;
    @Autowired
    private PasswordEncoder encoder;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error) {
        if (error != null)
            model.addAttribute("error", "Nesprávné jméno/heslo!");

        return "login";
    }

    @RequestMapping(value = {
            "/",
            "/welcome"
    }, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "index";
    }

    @RequestMapping(value = "/register")
    public String register(Model model) {
        JidelnaUser jidelnaUser = new JidelnaUser();
        model.addAttribute("jidelnaUser", jidelnaUser);
        return "register";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute JidelnaUser jidelnaUser, BindingResult bindingResult) {
        jidelnaUser.setRole(roleRepository.getByName("stravnik").orElseThrow(() -> new UsernameNotFoundException("User  not found")));
        jidelnaUser.setKredit(420);
        if(bindingResult.hasErrors()) {
                return "register";
        }
        jidelnaUser.setPassword(encoder.encode(jidelnaUser.getPassword()));
        userRepository.save(jidelnaUser);
        return "login";
    }
}
