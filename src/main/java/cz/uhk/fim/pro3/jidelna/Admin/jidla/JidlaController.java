package cz.uhk.fim.pro3.jidelna.Admin.jidla;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.ICenikRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IJidlaRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidlo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/Admin/jidla")
public class JidlaController {
    @Autowired
    private IJidlaRepository repo;
    @Autowired
    private ICenikRepository cRepo;


    @RequestMapping(value = {"/", "", "index"})
    public String index(Model model,
                        @RequestParam("page") Optional<Integer> page,
                        @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        int totalItems = (int) repo.count();
        int pages = (int) Math.ceil(totalItems / (double) pageSize);

        model.addAttribute("pages", pages);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("jidla", repo.getItemsPaged(currentPage, pageSize));

        return "Admin/jidla/index";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @RequestMapping(value = "/edit")
    public String edit(Model model, @RequestParam("id") int id) {
        model.addAttribute("ceny", cRepo.findAll());
        if (id == 0) {
            Jidlo jidlo = new Jidlo();
            model.addAttribute("jidlo", jidlo);
            return "Admin/jidla/edit";
        }
        model.addAttribute("jidlo", repo.getOne(id));
        return "Admin/jidla/edit";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @PostMapping(value = "/edit")
    public String edit(@Valid Jidlo jidlo, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        jidlo.setCenik(cRepo.getOne(jidlo.getCenik().getId()));
        model.addAttribute("ceny", cRepo.findAll());
        if (bindingResult.hasErrors()) {
            return "Admin/jidla/edit";
        }
        repo.save(jidlo);
        redirectAttributes.addAttribute("success", true);
        return "redirect:/Admin/jidla/index";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @RequestMapping(value = "/delete")
    public String delete(Model model, @RequestParam("id") int id, RedirectAttributes redirectAttributes) {
        repo.deleteById(id);
        redirectAttributes.addAttribute("success", true);
        return "redirect:/Admin/jidla/index";
    }
}
