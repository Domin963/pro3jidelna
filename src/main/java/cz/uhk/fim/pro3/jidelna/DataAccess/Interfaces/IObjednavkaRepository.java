package cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces;

import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Objednavka;

import java.time.LocalDate;
import java.util.List;

public interface IObjednavkaRepository extends ExtRepo<Objednavka, Integer> {
    List<Objednavka> findAllByTydenDatumAndJidelna_Id(LocalDate tydenDatum, int jidelnaId);
}
