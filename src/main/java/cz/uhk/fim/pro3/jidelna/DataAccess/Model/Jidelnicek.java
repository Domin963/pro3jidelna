package cz.uhk.fim.pro3.jidelna.DataAccess.Model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "jidelnicek")
public class Jidelnicek {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne()
    @JoinColumn(name = "jidlo_id")
    private Jidlo jidlo;

    @Column(nullable = false)
    @NotNull
    @Min(1)
    private int cisloNabidky;

    @Column(nullable = false)
    @NotNull
    private LocalDate datum;

    @ManyToOne()
    @JoinColumn(name = "jidelna_id")
    private Jidelna jidelna;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Jidlo getJidlo() {
        return jidlo;
    }

    public void setJidlo(Jidlo jidloId) {
        this.jidlo = jidloId;
    }

    public int getCisloNabidky() {
        return cisloNabidky;
    }

    public void setCisloNabidky(int cisloNabidky) {
        this.cisloNabidky = cisloNabidky;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public Jidelna getJidelna() {
        return jidelna;
    }

    public void setJidelna(Jidelna jidelnaId) {
        this.jidelna = jidelnaId;
    }
}
