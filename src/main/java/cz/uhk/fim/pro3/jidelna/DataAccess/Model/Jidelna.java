package cz.uhk.fim.pro3.jidelna.DataAccess.Model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "jidelny")
public class Jidelna {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotNull
    @Pattern(regexp="^[A-Ža-ž0-9].*\\S+.*$", message = "Může obsahovat pouze písmena a čísla")
    private String nazev;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }
}
