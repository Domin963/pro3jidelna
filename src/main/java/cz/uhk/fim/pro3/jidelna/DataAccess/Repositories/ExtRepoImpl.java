package cz.uhk.fim.pro3.jidelna.DataAccess.Repositories;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.ExtRepo;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

public class ExtRepoImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements ExtRepo<T, ID> {
    private EntityManager entityManager;
    private CriteriaBuilder builder;

    public ExtRepoImpl(@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    public List<T> getItemsPaged(int page, int count) {
        this.builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(getDomainClass());
        Root<T> root = query.from(getDomainClass());
        CriteriaQuery<T> select = query.select(root);

        TypedQuery<T> typedQuery = entityManager.createQuery(select);
        typedQuery.setFirstResult((page - 1) * count);
        typedQuery.setMaxResults(count);

        return typedQuery.getResultList();
    }

    @Override
    public T getLast() {
        this.builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(getDomainClass());
        Root<T> root = query.from(getDomainClass());
        query.orderBy(builder.desc(root.get("id")));
        CriteriaQuery<T> select = query.select(root);

        TypedQuery<T> typedQuery = entityManager.createQuery(select);
        typedQuery.setMaxResults(1);
        return typedQuery.getSingleResult();
    }
}
