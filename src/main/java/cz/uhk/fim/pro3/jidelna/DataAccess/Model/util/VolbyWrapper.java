package cz.uhk.fim.pro3.jidelna.DataAccess.Model.util;

import java.util.ArrayList;
import java.util.List;

public class VolbyWrapper {
    private int v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, id, page;

    public int getV1() {
        return v1;
    }

    public List<Integer> toList() {
        List<Integer> lstVolby = new ArrayList<>();
        lstVolby.add(v1);
        lstVolby.add(v2);
        lstVolby.add(v3);
        lstVolby.add(v4);
        lstVolby.add(v5);
        lstVolby.add(v6);
        lstVolby.add(v7);
        lstVolby.add(v8);
        lstVolby.add(v9);
        lstVolby.add(v10);
        lstVolby.add(v11);
        lstVolby.add(v12);
        lstVolby.add(v13);
        lstVolby.add(v14);
        lstVolby.add(v15);
        return lstVolby;
    }

    public void setV1(int v1) {
        this.v1 = v1;
    }

    public int getV2() {
        return v2;
    }

    public void setV2(int v2) {
        this.v2 = v2;
    }

    public int getV3() {
        return v3;
    }

    public void setV3(int v3) {
        this.v3 = v3;
    }

    public int getV4() {
        return v4;
    }

    public void setV4(int v4) {
        this.v4 = v4;
    }

    public int getV5() {
        return v5;
    }

    public void setV5(int v5) {
        this.v5 = v5;
    }

    public int getV6() {
        return v6;
    }

    public void setV6(int v6) {
        this.v6 = v6;
    }

    public int getV7() {
        return v7;
    }

    public void setV7(int v7) {
        this.v7 = v7;
    }

    public int getV8() {
        return v8;
    }

    public void setV8(int v8) {
        this.v8 = v8;
    }

    public int getV9() {
        return v9;
    }

    public void setV9(int v9) {
        this.v9 = v9;
    }

    public int getV10() {
        return v10;
    }

    public void setV10(int v10) {
        this.v10 = v10;
    }

    public int getV11() {
        return v11;
    }

    public void setV11(int v11) {
        this.v11 = v11;
    }

    public int getV12() {
        return v12;
    }

    public void setV12(int v12) {
        this.v12 = v12;
    }

    public int getV13() {
        return v13;
    }

    public void setV13(int v13) {
        this.v13 = v13;
    }

    public int getV14() {
        return v14;
    }

    public void setV14(int v14) {
        this.v14 = v14;
    }

    public int getV15() {
        return v15;
    }

    public void setV15(int v15) {
        this.v15 = v15;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
