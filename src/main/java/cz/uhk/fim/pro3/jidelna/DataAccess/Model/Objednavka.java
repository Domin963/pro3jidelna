package cz.uhk.fim.pro3.jidelna.DataAccess.Model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "objednavka")
public class Objednavka {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "tyden_datum")
    private LocalDate tydenDatum;

    @ManyToOne()
    @JoinColumn(name = "jidelna_id")
    private Jidelna jidelna;

    @ManyToOne()
    @JoinColumn(name = "pondeli_id")
    private Jidelnicek pondeli;
    @ManyToOne()
    @JoinColumn(name = "utery_id")
    private Jidelnicek utery;
    @ManyToOne()
    @JoinColumn(name = "streda_id")
    private Jidelnicek streda;
    @ManyToOne()
    @JoinColumn(name = "ctvrtek_id")
    private Jidelnicek ctvrtek;
    @ManyToOne()
    @JoinColumn(name = "patek_id")
    private Jidelnicek patek;

    public int getTotal() {
        int total = 0;
        if (pondeli != null) {
            total += pondeli.getJidlo().getCenik().getCena();
        }
        if (utery != null) {
            total += utery.getJidlo().getCenik().getCena();
        }
        if (streda != null) {
            total += streda.getJidlo().getCenik().getCena();
        }
        if (ctvrtek != null) {
            total += ctvrtek.getJidlo().getCenik().getCena();
        }
        if (patek != null) {
            total += patek.getJidlo().getCenik().getCena();
        }

        return total;
    }

    public Jidelna getJidelna() {
        return jidelna;
    }

    public void setJidelna(Jidelna jidelna) {
        this.jidelna = jidelna;
    }

    public LocalDate getTydenDatum() {
        return tydenDatum;
    }

    public void setTydenDatum(LocalDate tydenDatum) {
        this.tydenDatum = tydenDatum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Jidelnicek getPondeli() {
        return pondeli;
    }

    public void setPondeli(Jidelnicek pondeliId) {
        this.pondeli = pondeliId;
    }

    public Jidelnicek getUtery() {
        return utery;
    }

    public void setUtery(Jidelnicek uteryId) {
        this.utery = uteryId;
    }

    public Jidelnicek getStreda() {
        return streda;
    }

    public void setStreda(Jidelnicek stredaId) {
        this.streda = stredaId;
    }

    public Jidelnicek getCtvrtek() {
        return ctvrtek;
    }

    public void setCtvrtek(Jidelnicek ctvrtekId) {
        this.ctvrtek = ctvrtekId;
    }

    public Jidelnicek getPatek() {
        return patek;
    }

    public void setPatek(Jidelnicek patekId) {
        this.patek = patekId;
    }
}
