package cz.uhk.fim.pro3.jidelna.DataAccess.Security;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IRoleRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IUserRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.JidelnaUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        JidelnaUser user = userRepository.getByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User " + username + " not found"));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                getAuthorities(user));
    }
    private static Collection<? extends GrantedAuthority> getAuthorities(JidelnaUser user) {
        String role = user.getRole().getName();
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(role);
        return authorities;
    }
}
