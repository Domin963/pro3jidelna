package cz.uhk.fim.pro3.jidelna.DataAccess.Model;

import javax.persistence.*;

@Entity
@Table(name = "objednavky")
public class Objednavky {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne()
    @JoinColumn(name = "objednavka_id")
    private Objednavka objednavka;
    @ManyToOne()
    @JoinColumn(name = "user_id")
    private JidelnaUser user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Objednavka getObjednavka() {
        return objednavka;
    }

    public void setObjednavka(Objednavka objednavkaId) {
        this.objednavka = objednavkaId;
    }

    public JidelnaUser getUser() {
        return user;
    }

    public void setUser(JidelnaUser userId) {
        this.user = userId;
    }
}
