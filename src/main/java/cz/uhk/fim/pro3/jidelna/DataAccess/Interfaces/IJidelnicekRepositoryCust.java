package cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces;

import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidelnicek;

import java.time.LocalDate;
import java.util.List;

public interface IJidelnicekRepositoryCust {
    public List<Jidelnicek> getWeekJidelnicek(LocalDate start, LocalDate end, int jidelnaId);
}
