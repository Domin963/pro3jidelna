package cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces;

import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidelnicek;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface IJidelnicekRepository extends ExtRepo<Jidelnicek, Integer>, IJidelnicekRepositoryCust {
}
