package cz.uhk.fim.pro3.jidelna;

import cz.uhk.fim.pro3.jidelna.DataAccess.Repositories.ExtRepoImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "cz.uhk.fim.pro3.jidelna.DataAccess", repositoryBaseClass = ExtRepoImpl.class)
public class JidelnaApplication {
    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC")); //nastaveni kvuli MySQL serveru kvuli zmenam LocalDate
    }

    public static void main(String[] args) {
        SpringApplication.run(JidelnaApplication.class, args);
    }
}
