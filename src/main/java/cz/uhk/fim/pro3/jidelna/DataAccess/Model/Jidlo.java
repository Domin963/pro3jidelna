package cz.uhk.fim.pro3.jidelna.DataAccess.Model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "jidlo")
public class Jidlo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotNull
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "Může obsahovat pouze písmena")
    private String nazev;

    @Column(nullable = false)
    @NotNull
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "Může obsahovat pouze písmena")
    private String polevka;

    @Column(nullable = false)
    @NotNull
    @Pattern(regexp = "^[\\p{L} .'-]+$", message = "Může obsahovat pouze písmena")
    private String napoj;

    @ManyToOne()
    @JoinColumn(name = "cenik_id")
    private Cenik cenik;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public String getPolevka() {
        return polevka;
    }

    public void setPolevka(String polevka) {
        this.polevka = polevka;
    }

    public String getNapoj() {
        return napoj;
    }

    public void setNapoj(String napoj) {
        this.napoj = napoj;
    }

    public Cenik getCenik() {
        return cenik;
    }

    public void setCenik(Cenik cenik_id) {
        this.cenik = cenik_id;
    }
}
