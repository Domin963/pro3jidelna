package cz.uhk.fim.pro3.jidelna.Admin;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IUserRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.JidelnaUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;

@Controller
@RequestMapping("/Admin")
public class AdminController {
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private IUserRepository userRepository;

    @RequestMapping(value = {"/", "", "/welcome"}, method = RequestMethod.GET)
    public String index(Model model) {
        return "Admin/index";
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public RedirectView logout(Model model) {
        return new RedirectView("redirect:/welcome");
    }

    @RequestMapping(value = "/changepswd")
    public String changepswd(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JidelnaUser jidelnaUser = userRepository.getByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        model.addAttribute("jidelnaUser", jidelnaUser);
        return "/Admin/changepswd";
    }

    @PostMapping("/changepswd")
    public String changepswd(@Valid @ModelAttribute JidelnaUser jidelnaUser, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "/Admin/changepswd";
        }
        jidelnaUser.setPassword(encoder.encode(jidelnaUser.getPassword()));
        userRepository.save(jidelnaUser);
        redirectAttributes.addAttribute("success", true);
        return "redirect:/Admin/welcome";
    }
}
