package cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces;

import cz.uhk.fim.pro3.jidelna.DataAccess.Model.JidelnaRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IRoleRepository extends ExtRepo<JidelnaRole, Integer> {
    Optional<JidelnaRole> getByName(String roleName);
}
