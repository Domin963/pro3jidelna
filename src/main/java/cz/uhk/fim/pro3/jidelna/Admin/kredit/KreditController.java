package cz.uhk.fim.pro3.jidelna.Admin.kredit;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IUserRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.JidelnaUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/Admin/kredit")
public class KreditController {

    @Autowired
    IUserRepository userRepo;

    @RequestMapping(value = {"/", "", "index"})
    public String index(Model model) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        JidelnaUser user = userRepo.getByUsername(userDetails.getUsername()).get();
        model.addAttribute("kredit", user.getKredit());
        return "Admin/kredit/index";
    }

    @PostMapping(value = "add")
    public String add(@RequestParam int castka, RedirectAttributes redirectAttributes) {
        if (castka < 1) {
            redirectAttributes.addAttribute("error", true);
            return "redirect:/Admin/kredit/";
        }
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        JidelnaUser user = userRepo.getByUsername(userDetails.getUsername()).get();
        user.setKredit(user.getKredit() + castka);
        userRepo.save(user);
        redirectAttributes.addAttribute("success", true);
        return "redirect:";
    }
}
