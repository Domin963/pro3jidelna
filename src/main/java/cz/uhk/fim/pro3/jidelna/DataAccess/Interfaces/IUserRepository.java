package cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces;

import cz.uhk.fim.pro3.jidelna.DataAccess.Model.JidelnaUser;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface IUserRepository extends ExtRepo<JidelnaUser, Integer> {
    Optional<JidelnaUser> getByUsername(String userName);

    List<JidelnaUser> getAllByRole_Name(String role, Pageable pageable);

    Integer countAllByRole_Name(String role);
}
