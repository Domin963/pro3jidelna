package cz.uhk.fim.pro3.jidelna.Admin.cenik;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.ICenikRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Cenik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/Admin/cenik")
public class CenikController {
    @Autowired
    private ICenikRepository cenikRepo;

    @RequestMapping(value = {"/", "", "index"})
    public String index(Model model) {
        model.addAttribute("cenik", cenikRepo.findAll());
        return "Admin/cenik/index";
    }

    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping(value = "/edit")
    public String edit(Model model, @RequestParam("id")int id) {
        if(id==0){
            Cenik cenik = new Cenik();
            model.addAttribute("cenik", cenik);
            return "Admin/cenik/edit";
        }
        model.addAttribute("cenik", cenikRepo.getOne(id));
        return "Admin/cenik/edit";
    }
    @PreAuthorize("hasAuthority('admin')")
    @PostMapping(value = "/edit")
    public String edit(@Valid Cenik cenik, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "Admin/cenik/edit";
        }
            cenikRepo.save(cenik);
        redirectAttributes.addAttribute("success",true);
        return "redirect:/Admin/cenik/index";
    }
}
