package cz.uhk.fim.pro3.jidelna.Admin.jidelnicek;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.*;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidelnicek;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Objednavka;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Objednavky;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.util.VolbyWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Controller
@RequestMapping("/Admin/jidelnicek")
public class JidelnicekController {

    @Autowired
    IJidelnicekRepository repo;
    @Autowired
    IJidelnyRepository jRepo;
    @Autowired
    IJidlaRepository jidlaRepo;
    @Autowired
    IObjednavkaRepository objednavkaRepo;

    LocalDate start = LocalDate.now();

    @RequestMapping(value = {"/", "", "index"})
    public String index(Model model) {
        model.addAttribute("jidelny", jRepo.findAll());
        List<String> tydny = new ArrayList<>();
        tydny.add("Tento týden");
        tydny.add("Příští týden");
        tydny.add(LocalDate.now().plusWeeks(2).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        tydny.add(LocalDate.now().plusWeeks(3).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        model.addAttribute("tydny", tydny);
        return "Admin/jidelnicek/index";
    }

    @RequestMapping(value = "/pocty")
    public String pocty(Model model) {
        model.addAttribute("jidelny", jRepo.findAll());
        List<String> tydny = new ArrayList<>();
        tydny.add("Tento týden");
        tydny.add("Příští týden");
        tydny.add(LocalDate.now().plusWeeks(2).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        tydny.add(LocalDate.now().plusWeeks(3).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        model.addAttribute("tydny", tydny);
        return "Admin/jidelnicek/pocty";
    }

    @RequestMapping(value = "/showPocty/id={id}&page={page}", method = RequestMethod.GET)
    public String showPocty(Model model, @PathVariable("id") Optional<Integer> id, @PathVariable("page") Optional<Integer> page) {
        int currentPage = page.orElse(1);
        start = getLocalDate(currentPage, start);
        List<Objednavka> lstObj = objednavkaRepo.findAllByTydenDatumAndJidelna_Id(start, id.get());
        int[][] pocty = new int[5][3];
        for (Objednavka o : lstObj) {
            if (o.getPondeli() != null) {
                pocty[0][o.getPondeli().getCisloNabidky() - 1]++;
            }
            if (o.getUtery() != null) {
                pocty[1][o.getUtery().getCisloNabidky() - 1]++;
            }
            if (o.getStreda() != null) {
                pocty[2][o.getStreda().getCisloNabidky() - 1]++;
            }
            if (o.getCtvrtek() != null) {
                pocty[3][o.getCtvrtek().getCisloNabidky() - 1]++;
            }
            if (o.getPatek() != null) {
                pocty[4][o.getPatek().getCisloNabidky() - 1]++;
            }
        }
        model.addAttribute("pocty", pocty);
        model.addAttribute("idJidelny", id.get());
        model.addAttribute("currentPage", currentPage);
        return "Admin/jidelnicek/resultsPocty :: resultsList";
    }

    @RequestMapping(value = "/show/id={id}&page={page}", method = RequestMethod.GET)
    public String showJidelnicek(Model model, @PathVariable("id") Optional<Integer> id, @PathVariable("page") Optional<Integer> page) {
        //nacteni jidelnicku pro danou jidelnu
        int currentPage = page.orElse(1);
        LocalDate end = LocalDate.now();

        switch (currentPage) {
            case 1:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
                end = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY));
                break;
            case 2:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).plusWeeks(1);
                end = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY)).plusWeeks(1);
                break;
            case 3:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).plusWeeks(2);
                end = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY)).plusWeeks(2);
                break;
            case 4:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).plusWeeks(3);
                end = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY)).plusWeeks(3);
                break;
        }
        model.addAttribute("jidelnicky", repo.getWeekJidelnicek(start, end, id.get()));
        model.addAttribute("start", start);
        model.addAttribute("end", end);
        model.addAttribute("idJidelny", id.get());
        model.addAttribute("currentPage", currentPage);
        return "Admin/jidelnicek/results :: resultsList";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @RequestMapping(value = "/edit")
    public String edit(Model model, @RequestParam("id") int id) {
        model.addAttribute("jidelnicek", repo.getOne(id));
        model.addAttribute("jidla", jidlaRepo.findAll());
        return "Admin/jidelnicek/edit";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @PostMapping(value = "/edit")
    public String edit(Jidelnicek j) {
        Jidelnicek jid = repo.getOne(j.getId());
        jid.setJidlo(j.getJidlo());
        repo.save(jid);
        return "redirect:/Admin/jidelnicek/";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @RequestMapping(value = "/create")
    public String create(Model model, @RequestParam("id") int id, @RequestParam("page") int page) {
        VolbyWrapper volby = new VolbyWrapper();
        volby.setId(id);
        volby.setPage(page);
        model.addAttribute("volby", volby);
        model.addAttribute("jidla", jidlaRepo.findAll());
        return "Admin/jidelnicek/create";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @PostMapping(value = "/create")
    public String create(VolbyWrapper volby) {
        int cNabidky = 1;
        List<Integer> lstVolby = volby.toList();
        List<Jidelnicek> lstJidelnicek = new ArrayList<>();
        start = getLocalDate(volby.getPage(), start);
        for (int x = 0; x < 15; x++) {
            Jidelnicek j = new Jidelnicek();
            j.setJidelna(jRepo.getOne(volby.getId()));
            j.setDatum(start.plusDays(x / 3));
            j.setJidlo(jidlaRepo.getOne(lstVolby.get(x)));
            if (cNabidky > 3) {
                cNabidky = 1;
            }
            j.setCisloNabidky(cNabidky);
            lstJidelnicek.add(j);
            cNabidky++;
        }
        repo.saveAll(lstJidelnicek);
        return "redirect:/Admin/jidelnicek/";
    }

    private LocalDate getLocalDate(@RequestParam("page") int page, LocalDate start) {
        switch (page) {
            case 1:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
                break;
            case 2:
                start = LocalDate.now().plusWeeks(1).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
                break;
            case 3:
                start = LocalDate.now().plusWeeks(2).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
                break;
            case 4:
                start = LocalDate.now().plusWeeks(3).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
                break;
        }
        return start;
    }

    /**
     * Metoda pro nahodne vygenerovani jidelnicku na dany tyden
     *
     * @param id   id jidelny
     * @param page urceni tydnu (1-4)
     * @return vrati na vypis jidelnicku
     */
    @PreAuthorize("hasAnyAuthority('admin', 'zamestnanec')")
    @RequestMapping(value = "/randomJidelnicek")
    public String randomJidelnicek(@RequestParam("id") int id, @RequestParam("page") int page) {
        Random rnd = new Random();
        start = getLocalDate(page, start);
        int rndNum;
        int range = jidlaRepo.getLast().getId(); //rozsah vsech moznych jidel
        List<Integer> lstUsed = new ArrayList(); //seznam pouzitych ID jidel
        lstUsed.add(jidlaRepo.getByName("nic").getId()); //vynechani prazdneho jidla
        List<Jidelnicek> lstJidelnicek = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            for (int u = 1; u < 4; u++) {
                Jidelnicek j = new Jidelnicek();
                j.setJidelna(jRepo.getOne(id));
                j.setDatum(start);
                j.setCisloNabidky(u);
                do {
                    rndNum = rnd.nextInt(range) + 1;
                } while (lstUsed.contains(rndNum));
                //pokud jidlo neexistuje, zkusi jine ID
                while (!jidlaRepo.existsById(rndNum)) {
                    lstUsed.add(rndNum);
                    rndNum = rnd.nextInt(range) + 1;
                }
                lstUsed.add(rndNum);
                j.setJidlo(jidlaRepo.getOne(rndNum));
                lstJidelnicek.add(j);
            }
            start = start.plusDays(1);
        }
        repo.saveAll(lstJidelnicek);
        return "redirect:/Admin/jidelnicek/";
    }
}
