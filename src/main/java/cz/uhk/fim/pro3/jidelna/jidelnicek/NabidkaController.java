package cz.uhk.fim.pro3.jidelna.jidelnicek;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IJidelnicekRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IJidelnyRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IJidlaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/nabidka")
public class NabidkaController {

    @Autowired
    IJidelnicekRepository repo;
    @Autowired
    IJidelnyRepository jRepo;
    @Autowired
    IJidlaRepository jidlaRepo;
    LocalDate start = LocalDate.now();

    @RequestMapping(value = {"/", "index"})
    public String index(Model model) {
        model.addAttribute("jidelny", jRepo.findAll());
        List<String> tydny = new ArrayList<>();
        tydny.add("Tento týden");
        tydny.add("Příští týden");
        model.addAttribute("tydny", tydny);
        return "nabidka/index";
    }

    @RequestMapping(value = "/show/id={id}&page={page}", method = RequestMethod.GET)
    public String showJidelnicek(Model model, @PathVariable("id") Optional<Integer> id, @PathVariable("page") Optional<Integer> page) {
        //nacteni jidelnicku pro danou jidelnu
        int currentPage = page.orElse(1);
        LocalDate end = LocalDate.now();

        switch (currentPage) {
            case 1:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
                end = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY));
                break;
            case 2:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).plusWeeks(1);
                end = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY)).plusWeeks(1);
                break;
        }
        model.addAttribute("jidelnicky", repo.getWeekJidelnicek(start, end, id.get()));
        model.addAttribute("start", start);
        model.addAttribute("end", end);
        model.addAttribute("idJidelny", id.get());
        model.addAttribute("currentPage", currentPage);
        return "nabidka/results :: resultsList";
    }
}
