package cz.uhk.fim.pro3.jidelna.DataAccess.Repositories;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IJidelnicekRepositoryCust;
import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IJidlaRepositoryCust;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidelnicek;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidlo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class IJidlaRepositoryImpl implements IJidlaRepositoryCust {

    @PersistenceContext
    EntityManager entityManager;
    CriteriaBuilder builder;
    private CriteriaQuery<Jidelnicek> query;

    @Override
    public Jidlo getByName(String name) {

        this.builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Jidlo> query = builder.createQuery(Jidlo.class);
        Root<Jidlo> root = query.from(Jidlo.class);

        List<Predicate> predicates = new ArrayList<Predicate>();

        predicates.add(builder.equal(root.get("nazev"), name));

        query.select(root).where(predicates.toArray(new Predicate[]{}));

        return entityManager.createQuery(query).getSingleResult();
    }
}
