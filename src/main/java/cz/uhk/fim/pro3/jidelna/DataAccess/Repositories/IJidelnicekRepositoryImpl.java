package cz.uhk.fim.pro3.jidelna.DataAccess.Repositories;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IJidelnicekRepositoryCust;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidelnicek;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class IJidelnicekRepositoryImpl implements IJidelnicekRepositoryCust {
    @PersistenceContext
    EntityManager entityManager;
    CriteriaBuilder builder;

    @Override
    public List<Jidelnicek> getWeekJidelnicek(LocalDate start, LocalDate end, int jidelna) {

        this.builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Jidelnicek> query = builder.createQuery(Jidelnicek.class);
        Root<Jidelnicek> root = query.from(Jidelnicek.class);

        List<Predicate> predicates = new ArrayList<Predicate>();

        predicates.add(builder.between(root.get("datum"), start, end));
        predicates.add(builder.equal(root.get("jidelna").<String>get("id"), jidelna));

        query.select(root).where(predicates.toArray(new Predicate[]{})).orderBy(builder.asc(root.get("datum")), builder.asc(root.get("cisloNabidky")));

        return entityManager.createQuery(query).getResultList();
    }
}
