package cz.uhk.fim.pro3.jidelna.Admin.jidelny;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.IJidelnyRepository;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidelna;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/Admin/jidelny")
public class JidelnyController {
    @Autowired
    private IJidelnyRepository jRepo;


    @RequestMapping(value = {"/", "", "index"})
    public String index(Model model,
                        @RequestParam("page") Optional<Integer> page,
                        @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        int totalItems = (int) jRepo.count();
        int pages = (int) Math.ceil(totalItems / (double) pageSize);

        model.addAttribute("pages", pages);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("jidelny", jRepo.getItemsPaged(currentPage, pageSize));

        return "Admin/jidelny/index";
    }

    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping(value = "/edit")
    public String edit(Model model, @RequestParam("id") int id) {
        if (id == 0) {
            Jidelna jidelna = new Jidelna();
            model.addAttribute("jidelna", jidelna);
            return "Admin/jidelny/edit";
        }
        model.addAttribute("jidelna", jRepo.getOne(id));
        return "Admin/jidelny/edit";
    }

    @PreAuthorize("hasAuthority('admin')")
    @PostMapping(value = "/edit")
    public String edit(@Valid Jidelna jidelna, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "Admin/jidelny/edit";
        }
        jRepo.save(jidelna);
        redirectAttributes.addAttribute("success", true);
        return "redirect:/Admin/jidelny/index";
    }

    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping(value = "/delete")
    public String delete(Model model, @RequestParam("id") int id, RedirectAttributes redirectAttributes) {
        jRepo.deleteById(id);
        redirectAttributes.addAttribute("success", true);
        return "redirect:/Admin/jidelny/index";
    }
}
