package cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface ExtRepo<T, ID extends Serializable> extends JpaRepository<T, ID> {
    public List<T> getItemsPaged(int count, int page);

    public T getLast();
}
