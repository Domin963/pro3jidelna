package cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces;

import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Objednavky;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;

public interface IObjednavkyRepository extends ExtRepo<Objednavky, Integer> {
    List<Objednavky> findAllByUser_UsernameAndObjednavka_TydenDatumAndObjednavka_Jidelna_Id(String username, LocalDate tydenDatum, int jidelnaId);

    List<Objednavky> findAllByUser_UsernameOrderByObjednavka_TydenDatumDesc(String username, Pageable pageable);

    Objednavky findByObjednavkaId(int objId);

    int countAllByUser_Username(String username);
}
