package cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces;

import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Cenik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

public interface ICenikRepository extends ExtRepo<Cenik, Integer> {
}
