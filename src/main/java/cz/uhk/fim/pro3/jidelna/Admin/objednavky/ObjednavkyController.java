package cz.uhk.fim.pro3.jidelna.Admin.objednavky;

import cz.uhk.fim.pro3.jidelna.DataAccess.Interfaces.*;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.JidelnaUser;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Jidelnicek;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Objednavka;
import cz.uhk.fim.pro3.jidelna.DataAccess.Model.Objednavky;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/Admin/objednavky")
public class ObjednavkyController {

    @Autowired
    IJidelnyRepository jidelnaRepo;
    @Autowired
    IObjednavkyRepository objednavkyRepository;
    @Autowired
    IJidelnicekRepository jidlaRepo;
    @Autowired
    IObjednavkaRepository objednavkaRepository;
    @Autowired
    IUserRepository userRepo;

    LocalDate start = LocalDate.now();
    LocalDate end = LocalDate.now();

    @RequestMapping(value = {"/", "", "index"})
    public String index(Model model) {
        model.addAttribute("jidelny", jidelnaRepo.findAll());
        List<String> tydny = new ArrayList<>();
        tydny.add("Tento týden");
        tydny.add("Příští týden");
        model.addAttribute("tydny", tydny);
        return "Admin/objednavky/index";
    }

    @RequestMapping(value = "/show/id={id}&page={page}", method = RequestMethod.GET)
    public String showObjednavky(Model model, @PathVariable("id") Optional<Integer> id, @PathVariable("page") Optional<Integer> page) {
        //nacteni objednavek pro danou jidelnu
        int currentPage = page.orElse(1);
        switch (currentPage) {
            case 1:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
                end = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY));
                break;
            case 2:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).plusWeeks(1);
                end = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY)).plusWeeks(1);
                break;
        }
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Objednavky> objednavky = objednavkyRepository.findAllByUser_UsernameAndObjednavka_TydenDatumAndObjednavka_Jidelna_Id(userDetails.getUsername(), start, id.get());
        Objednavky obj = new Objednavky();
        for (Objednavky o : objednavky) {
            if (o.getObjednavka().getJidelna().getId() == id.get() && o.getObjednavka().getTydenDatum().equals(start)) {
                obj = o;
            }
        }
        List<Jidelnicek> jidelnicky = jidlaRepo.getWeekJidelnicek(start, end, id.get());
        model.addAttribute("canCreate", LocalDate.now().getDayOfWeek().getValue() < 5 || currentPage > 1 && jidelnicky.isEmpty() == false); //uprava tohoto tydne do patku, pristi tyden
        model.addAttribute("start", start);
        model.addAttribute("idJidelny", id.get());
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("objednavka", obj);
        return "Admin/objednavky/results :: resultsList";
    }

    @RequestMapping(value = "/create")
    public String create(Model model, @RequestParam("objId") Optional<Integer> objId, @RequestParam("id") int id, @RequestParam("page") int page) {
        switch (page) {
            case 1:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
                end = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY));
                break;
            case 2:
                start = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).plusWeeks(1);
                end = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY)).plusWeeks(1);
                break;
        }
        model.addAttribute("jidelnicky", jidlaRepo.getWeekJidelnicek(start, end, id));
        model.addAttribute("currentPage", page);
        if (objId.isPresent()) {
            model.addAttribute("objednavka", objednavkyRepository.getOne(objId.get()).getObjednavka());
        } else {
            Objednavka o = new Objednavka();
            o.setTydenDatum(start);
            o.setJidelna(jidelnaRepo.getOne(id));
            model.addAttribute("objednavka", o);
        }
        return "Admin/objednavky/create";
    }

    @PostMapping(value = "/create")
    public String create(Objednavka objednavka, RedirectAttributes redirectAttributes) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        JidelnaUser user = userRepo.getByUsername(userDetails.getUsername()).get();
        objednavka = loadJidelnicky(objednavka);
        int total = 0;
        boolean isNew = false;
        //zmena objednavky
        if (objednavka.getId() != 0) {
            Objednavka old = objednavkaRepository.getOne(objednavka.getId());
            int oldTotal = old.getTotal();
            total = objednavka.getTotal() - oldTotal;
            //po preuctovani zaporny kredit
            if (user.getKredit() - total < 0) {
                redirectAttributes.addAttribute("errorInsufficientFunds", true);
                return "redirect:/Admin/objednavky/";
            }
        } else {//nova objednavka
            isNew = true;
            total = objednavka.getTotal();
            //pokud je cena objednavky vyssi nez kredit operace neprobehne
            if (user.getKredit() < total) {
                redirectAttributes.addAttribute("errorInsufficientFunds", true);
                return "redirect:/Admin/objednavky/";
            }
        }
        //odecteni kreditu, ulozeni zmen
        user.setKredit(user.getKredit() - total);
        userRepo.save(user);
        objednavkaRepository.save(objednavka);
        //vytvoreni objektu Objednavky, pokud je objednavka nova
        if (isNew) {
            Objednavky o = new Objednavky();
            o.setObjednavka(objednavka);
            o.setUser(user);
            objednavkyRepository.save(o);
        }
        redirectAttributes.addAttribute("success", true);
        return "redirect:/Admin/objednavky/";
    }

    @RequestMapping(value = "/delete")
    public String delete(@RequestParam("idObj") int idObj, RedirectAttributes redirectAttributes) {
        Objednavky obj = objednavkyRepository.getOne(idObj);
        Objednavka o = objednavkaRepository.getOne(obj.getObjednavka().getId());
        JidelnaUser user = obj.getUser();
        user.setKredit(user.getKredit() + obj.getObjednavka().getTotal());
        objednavkyRepository.delete(obj);
        objednavkaRepository.delete(o);
        redirectAttributes.addAttribute("success", true);
        return "redirect:/Admin/objednavky/index";
    }

    @RequestMapping(value = "/history")
    public String history(@RequestParam Optional<Integer> page, @RequestParam Optional<Integer> size, Model model) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        int totalItems = 0;
        totalItems = objednavkyRepository.countAllByUser_Username(userDetails.getUsername());
        int pages = (int) Math.ceil(totalItems / (double) pageSize);
        Pageable pageAble = PageRequest.of(currentPage - 1, pageSize);
        List<Objednavky> objednavky = objednavkyRepository.findAllByUser_UsernameOrderByObjednavka_TydenDatumDesc(userDetails.getUsername(), pageAble);
        model.addAttribute("objednavky", objednavky);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pages", pages);
        return "Admin/objednavky/history";
    }

    @RequestMapping(value = "/detail")
    public String detail(@RequestParam int id, Model model) {
        model.addAttribute("objednavka", objednavkyRepository.getOne(id));
        return "Admin/objednavky/detail";
    }

    private Objednavka loadJidelnicky(Objednavka o) {
        if (o.getPondeli() != null) {
            o.setPondeli(jidlaRepo.getOne(o.getPondeli().getId()));
        }
        if (o.getUtery() != null) {
            o.setUtery(jidlaRepo.getOne(o.getUtery().getId()));
        }
        if (o.getStreda() != null) {
            o.setStreda(jidlaRepo.getOne(o.getStreda().getId()));
        }
        if (o.getCtvrtek() != null) {
            o.setCtvrtek(jidlaRepo.getOne(o.getCtvrtek().getId()));
        }
        if (o.getPatek() != null) {
            o.setPatek(jidlaRepo.getOne(o.getPatek().getId()));
        }
        return o;
    }
}